# Tic Tac Toe

A Simple (Dumb) tic tac toe game, with scoring and a top score list view.

## Getting Started & Prerequisites

Make sure mongo is installed and running on port 27017 by following the steps in this link:
“https://docs.mongodb.com.

Make sure node and Angular cli@7.3.9 are installed.
You can download and install node from here: 
“https://nodejs.org/en/",
then install the angular cli by using the following command 
in the command line\ terminal: 
“npm install -g @angular/cli“

To run the server:  open command line inside the server folder and run the following commands:
“npm i”
“node server.js”

To run the client: open command line inside the client folder and run the following commands:
“npm i”
“ng serve”

open your browser at 127.0.0.1:4200.

enjoy :)

## Architecture

This project was built using a node js backend server with mongodb as the DB(although not really implemented) and angular 7 for the frontend.

For now I created one module,the game module that handles all the game related features, including a game service where most of the buisiness logic is handled.

I also created a configuration folder which holds the configurations needed for server
What is missing is the Game persistency, this would have been implemented by using the mongoose game scheme and and related CRUD routes for that task i.e. getGameById in the gameController.js file.

In Addition In order to make the game available online I would have created a socket io file and used sockets for connections to the server in order for both online users to communicate simultaneously.
As for the smart bot, I read a really interesting article(https://www.codementor.io/rohitagrawalofficialmail/playing-tic-tac-toe-using-reinforcement-learning-x5rf9xvey) about solving this issue with "Reinforcement Learning" which looks really interesting and if I had the time or needed to create a smart bot for production I would have given this a shot.
Regarding design - Instead of alerts to pass information to the users I would have created modals / dialogs as for the score board list modal and off course enhanced the UX/ UI on each.

## Author

* **Jonathan Kredo** *


