import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  title: string;
  animal: string;
}

@Component({
  selector: 'app-scoreBoard-modal',
  templateUrl: './scoreBoard-modal.component.html',
  styleUrls: ['./scoreBoard-modal.component.css']
})
export class ScoreBoardModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ScoreBoardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onThanksClick(): void {
    this.dialogRef.close();
  }

}
