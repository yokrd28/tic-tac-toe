import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreBoardModalComponent } from './scoreBoard-modal.component';

describe('ScoreBoardModalComponent', () => {
  let component: ScoreBoardModalComponent;
  let fixture: ComponentFixture<ScoreBoardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreBoardModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreBoardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
