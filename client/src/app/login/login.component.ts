import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm = this.fb.group({
    nickname: ['', Validators.required]
  })
  public error;
  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    
  }

  onSubmit(){
    this.router.navigateByUrl('/game', { state: {nickname: this.loginForm.value.nickname }});
  }

}
