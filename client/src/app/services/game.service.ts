import { Injectable } from '@angular/core';
import { Player } from '../game/game.component';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {API_URL} from '../env';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  gameGrid: [
    ['blank', 'blank','blank'],
    ['blank', 'blank', 'blank'],
    ['blank', 'blank', 'blank']
  ];
  topPlayers: Player[] = [];
  constructor(private http: HttpClient) { 
  }

  private static _handleError(err: HttpErrorResponse | any) {
    return throwError( err ); 
  }

  nextStep(id?, symbolPath?): Observable<[]>{
    let positionId = id ? id : "";
    let symbol = symbolPath ? this.getSymbolFromPath(symbolPath) : "";
    return this.http
      .get<[]>(`${API_URL}/game/nextStep`, {
        params: {
            positionId,
            symbol
          }
        }
      )
      .pipe(catchError(GameService._handleError));
  }

  updateTopPlayers(players){
    //todo: update DB
    if(this.topPlayers.length === 0){
      this.topPlayers.push(...players);
    }
    else {
      for(let i = 0; i < players.length; i++){
        let found = this.topPlayers.find(currPlayer => {
          return currPlayer.nickname == players[i].nickname;
        });
        if(found){
          found.score = players[i].score;
        }
      }
    }
    this.topPlayers.sort((player1, player2) => (player1.score > player2.score) ? -1 : 1);
  }

  startNewGame(): Observable<string>{
    return this.http
      .get<string>(`${API_URL}/game/new`)
      .pipe(catchError(GameService._handleError));
  }

  getSymbolFromPath(symbolPath){
    return symbolPath.split("/").pop().split(".")[0]
  }
}
