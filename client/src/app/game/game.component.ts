import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, from } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from '../services/game.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ScoreBoardModalComponent } from '../scoreBoard-modal/scoreBoard-modal.component';

export interface Player{
  nickname: string,
  symbol?: string,
  score: number
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  
  state: object;
  players: Array<Player>;
  player1: Player;
  player2: Player;
  currentTurn: Player;
  loading: boolean = false;
  gameGrid;
  x_img_path = "../../assets/images/X.png";
  o_img_path = "../../assets/images/O.png";
  blank_img_path = "../../assets/images/blank.png";

  constructor(public activatedRoute: ActivatedRoute, private router: Router,
       public dialog: MatDialog, private gameService: GameService) {
    this.state = this.router.getCurrentNavigation().extras.state;
    if(this.state && this.state['nickname']){
      this.player1 = {
        nickname: this.state['nickname'],
        score: 0,
        symbol: ''
      };
      this.player2 = {
        nickname: 'Bot',
        score: 0,
        symbol: ''
      };
    }
    else {
      this.router.navigateByUrl("/login");
    }    
  }

  showScoreBoard(){
    const dialogRef = this.dialog.open(ScoreBoardModalComponent, {
      width: '250px',
      data: this.gameService.topPlayers
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    // });
  }

  selectSymbol(symbol1){
    if(this.player1){      
      this.players = [this.player1, this.player2];
    }
    this.loading = true;
    this.gameService.startNewGame()
    .subscribe(res => {
      this.loading = false;
      if(symbol1 === 'X'){
        this.player1.symbol = this.x_img_path;
        this.player2.symbol = this.o_img_path;
        alert("You start.. good luck");
        this.currentTurn = this.players[0];
      }    
      else {
        alert("Bot starts.. good luck");
        this.player1.symbol = this.o_img_path;
        this.player2.symbol = this.x_img_path;
        this.changeTurn();
        //todo: call server to get bot's move.
        this.playTurn();
      }
    }, err => {
      this.loading = false;
      console.log(JSON.stringify(err));
      alert("Could not start a new game. please try again later...");
    });
  }

  changeTurn(){
    this.players.reverse();
    this.currentTurn = this.players[0];
  }

  playTurn(event?){
    // console.log(JSON.stringify(event));    
    try{
      let position = "";
      let symbol = "";
      if(event && event.srcElement.currentSrc.includes("blank")){
        this.loading = true;
        //click allowed, place users symbol in clicked square
        event.currentTarget.firstElementChild.src = this.player1.symbol;
        this.changeTurn();
        position = event.currentTarget.id;
        symbol = this.player1.symbol;
      }
      this.gameService.nextStep(position, symbol)
      .subscribe(res => {
        if(res.hasOwnProperty("position")){
          let id = res["position"][0] + "_" + res["position"][1];
          let currSquare = document.getElementById(id);
          (currSquare.firstElementChild as HTMLImageElement).src = this.player2.symbol;
          this.changeTurn();
        }
        else if(res.hasOwnProperty("result")){
          let result = res["result"];
          if(result === "draw"){
            alert("It's a Draw.., try again..");
            this.player1.score += 10;
            this.player2.score += 10;
          }
          else if(this.player1.symbol.includes(result)){
            alert(this.player1.nickname + " Wins! Congratulations");
            this.player1.score += 100;
          }
          else {
            alert("Bot Wins! Try again");
            this.player2.score += 100;
          }
          this.resetGame();
          this.gameService.updateTopPlayers(this.players);
        }
        this.loading = false;
      }, err => {
        this.loading = false;
        console.log(JSON.stringify(err));
      });
    }
    catch (err){
      console.log(JSON.stringify(err));
    }    
  }

  resetGame() {
    this.loading = true;
    this.player1.symbol = "";
    this.player2.symbol = "";
    let squareIds = ["0_0", "0_1", "0_2", "1_0", "1_1", "1_2", "2_0", "2_1", "2_2"];
    for(let i = 0; i < squareIds.length; i++){
      let el = document.getElementById(squareIds[i]);
      (el.firstElementChild as HTMLImageElement).src = this.blank_img_path;
    }
    this.currentTurn = undefined;
  }
  ngOnInit() {
    this.gameGrid = this.gameService.gameGrid;
  }

}
