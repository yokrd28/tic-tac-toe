'use strict';

let mongoose = require('mongoose'),
  Game = mongoose.model('Game'),
  gameService = require('../services/gameService');

//for now the grid is handled here
let gameGrid = [];


//retrieves game by id in order to replay game   
exports.getGameById = function(req, res){ 
  let id = req.params.id
  //checks if package is cached
  Game.find({id}, (err, resGame) => {
      if (err) {
          res.send(err);  
      }  
      if(resGame){
        res.status(200).json(resGame);
      }
      else {
        res.status(202).json({message: "Could not find game."});
      }
  });
};

exports.newGrid = function(req, res){
  gameGrid = [
    ['blank', 'blank','blank'],
    ['blank', 'blank', 'blank'],
    ['blank', 'blank', 'blank']
  ];
  res.status(200).send({message:"Created new grid"});
}

exports.nextStep = function(req, res){
  let position = "";
  let symbol = "blank";
  if(req.query && req.query["positionId"]){
    position = gameService.getPostionArrayFrom(req.query["positionId"]);
    symbol = req.query["symbol"];
    //Update game grid with user's move..
    gameGrid[position[0]][position[1]] = symbol;
    //Todo: Save move in game history..
    //calculate bots move
    res.status(200).send(gameService.calculateNextStep(gameGrid, symbol));
  }  
  else {
    res.status(200).send(gameService.calculateNextStep(gameGrid, symbol));
  }
};

