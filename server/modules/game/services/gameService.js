'use strict';

function getPostionArrayFrom(id){
    return id.split("_");
}

function calculateNextStep(grid, usersSymbol){
    let botsSymbol = getBotsSymbol(usersSymbol);
    let symbolLocations = getSymbolLocations(grid);
    let botsLocations;
    let usersLocations;
    if(botsSymbol === 'X'){
        botsLocations = symbolLocations[0];
        usersLocations = symbolLocations[1];
    }
    else{
        botsLocations = symbolLocations[1];
        usersLocations = symbolLocations[0];
    }
    //check if user won
    let result = checkForWin(grid);
    if(result === 'X' || result === 'O'){
        return {result};
    }
    let botsPosition;
    let randIdx;
    if(usersLocations.length > 1){
        //todo: need to check locations and block if needed
        //for now just choose a position randomly
        randIdx = chooseRandomPosition(symbolLocations[2]);
    }
    else {
        //todo: need to check bots locations and try and win

        //choose a position randomly
        randIdx = chooseRandomPosition(symbolLocations[2]);        
    }
    botsPosition = symbolLocations[2][randIdx];
    botsLocations.push(botsPosition);
    symbolLocations[2].splice(randIdx, 1);
    grid[botsPosition[0]][botsPosition[1]] = botsSymbol;
    //check for wins or draw
    if(symbolLocations[0].length < 3 || symbolLocations[1].length < 3){
        return {position: botsPosition};
    }
    else {
        let result = checkForWin(grid);
        //check for draw
        if(!result && symbolLocations[2].length === 0){
            return{result: "draw"};
        }
        else {
            return result ? {result} : {position: botsPosition};
        }        
    }    
}

function chooseRandomPosition(options){
    return Math.floor(Math.random() * options.length)
}

function checkForWin(grid){
    let winningSymbol;
    winningSymbol = checkDiagonals(grid);
    let checksArr = [checkRows, checkColumns];
    let i = 0;
    while(!winningSymbol && i < checksArr.length){
        winningSymbol = checksArr[i](grid);
        i++;
    }
    return winningSymbol;
}

function checkDiagonals(grid){
    if((((grid[0][0] === grid[1][1]) && (grid[1][1] === grid[2][2])) || 
        ((grid[0][2] === grid[1][1]) && (grid[1][1] === grid[2][0]))) &&
        (grid[1][1] !== 'blank')){
             return grid[1][1];
    }
    else {
        return false;
    }
}

function checkRows(grid){
    //first row
    if((((grid[0][0] === grid[0][1]) && (grid[0][1] === grid[0][2]) && 
        (grid[0][1] !== 'blank')))){
            return grid[0][1];
    }
    //second row
    else if((((grid[1][0] === grid[1][1]) && (grid[1][1] === grid[1][2]) &&
        (grid[1][1] !== 'blank')))){
            return grid[1][1];
    }
    //third row
    else if((((grid[2][0] === grid[2][1]) && (grid[2][1] === grid[2][2]) &&
     (grid[2][1] !== 'blank')))){
        return grid[2][1];
    }
    else {
        return false;
    }
}

function checkColumns(grid){
    //first column
    if((((grid[0][0] === grid[1][0]) && (grid[1][0] === grid[2][0]) && 
        (grid[1][0] !== 'blank')))){
            return grid[1][0];
    }
    //second column
    else if(((grid[0][1] === grid[1][1]) && (grid[1][1] === grid[2][1]) &&
        (grid[1][1] !== 'blank'))){
            return grid[1][1];
    }
    //third column
    else if(((grid[0][2] === grid[1][2]) && (grid[1][2] === grid[2][2]) &&
        (grid[1][2] !== 'blank'))){
            return grid[1][2];
    }
    else {
        return false;
    }
}

function getBotsSymbol(usersSymbol){
    if(usersSymbol === 'X'){
      return 'O';
    }
    else {
      return 'X';
    }
}

function getSymbolLocations(grid){
    let xArr = [];
    let oArr = [];
    let blankArr = [];
    for(let i = 0; i < grid.length; i++){
        for(let j = 0; j < 3; j++){
            if(grid[i][j] === 'X'){
                xArr.push([i, j]);
            }
            else if(grid[i][j] === 'O'){
                oArr.push([i, j]);
            }
            else {
                blankArr.push([i, j]);
            }
        }
    }
    return [xArr, oArr, blankArr];
}


module.exports = {
    getPostionArrayFrom: getPostionArrayFrom,
    calculateNextStep: calculateNextStep
}