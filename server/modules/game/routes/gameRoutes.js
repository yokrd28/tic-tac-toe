'use strict';

module.exports = function(app) {
  let gameController = require('../controllers/gameController');

  // app.route('/game/:id')
  //   .get(gameController.getGame);

  app.route('/game/nextStep')
    .get(gameController.nextStep);  
  
  app.route('/game/new')
    .get(gameController.newGrid);
};