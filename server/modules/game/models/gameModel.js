'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let GameSchema = new Schema({
  players: {
    type: [],
    required: true
  },
  CreatedAt: {
    type: Date,
    default: Date.now
  },
  moves: []
});

module.exports = mongoose.model('Game', GameSchema);