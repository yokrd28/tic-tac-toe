let express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Game = require('./modules/game/models/gameModel'),
  bodyParser = require('body-parser');
  
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/TicTacToeDB', { useNewUrlParser: true, useCreateIndex: true }); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(enableCors);


var routes = require('./modules/game/routes/gameRoutes');
routes(app);


app.listen(port);


console.log('Tic Tac Toe game server started on: ' + port);

function enableCors(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}
